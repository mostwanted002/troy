/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const crypto = require('crypto')

const yes = ['yes', 'y', 'ye', 'yeah', 'yup', 'yea', 'ya', 'correct']
const no = ['no', 'n', 'nah', 'nope', 'nop', 'fuck off']

module.exports = class Helper {

    static formatNumber(number, minimumFractionDigits = 0) {
        return Number.parseFloat(number).toLocaleString(undefined, {
            minimumFractionDigits,
            maximumFractionDigits: 2,
        })
    }

    static shorten(text, maxLength = 2000) {
        if (text.length > maxLength) {
            return `${text.substr(0, maxLength - 3)}...`
        }
        else {
            return text
        }
    }

    static trimArray(array, maxLength = 10) {
        if (array.length > maxLength) {
            const len = array.length - maxLength
            array = array.slice(0, maxLength)
            array.push(`${len} more...`)
        }
        return array
    }

    static embedURL(title, url, display) {
        return `[${title}](${url.replaceAll(')', '%29')}${display ? ` "${display}"` : ''})`
    }

    static list(arr, conj = 'and') {
        const len = arr.length
        if (len === 0) return ''
        if (len === 1) return arr[0]
        return `${arr.slice(0, -1).join(', ')}${len > 1 ? `${len > 2 ? ',' : ''} ${conj} ` : ''}${arr.slice(-1)}`
    }

    static async verify(channel, user, { time = 30000, extraYes = [], extraNo = [] } = {}) {
        const filter = res => {
            const value = res.content.toLowerCase()
            return (user ? res.author.id === user.id : true)
                && (yes.includes(value) || no.includes(value) || extraYes.includes(value) || extraNo.includes(value))
        }
        const verify = await channel.awaitMessages(filter, { max: 1, time })
        if (!verify.size) return 0
        const choice = verify.first().content.toLowerCase()
        if (yes.includes(choice) || extraYes.includes(choice)) return true
        if (no.includes(choice) || extraNo.includes(choice)) return false
        return false
    }

    static shuffle(array) {
        const arr = array.slice(0)
        for (let i = arr.length - 1; i >= 0; i--) {
            const j = Math.floor(Math.random() * (i + 1))
            const temp = arr[i]
            arr[i] = arr[j]
            arr[j] = temp
        }
        return arr
    }
    static hash(text, algorithm) {
        return crypto.createHash(algorithm).update(text).digest('hex')
    }
}