/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
const Command = require('../../base/Command')

module.exports = class RPSCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'rps',
            description: 'Play Rock, Paper, Scissors with bot.',
            examples: ['rps <your choice>'],
            group: 'fun',
            memberName: 'rps',
            args: [{
                key: 'userInput',
                prompt: 'Select your choice, Is it Rock, Paper or Scissors?',
                type: 'string',
            }],
        })
    }

    run(message, { userInput }) {

        const rps = ['rock', 'paper', 'scissors']
        const choice = rps[Math.floor(Math.random() * rps.length)]
        const userChoice = userInput.toLowerCase()
        const msgArray = []

        msgArray.push(`You chose ***${userChoice.toUpperCase()}***. I choose ***${choice.toUpperCase()}***.`)

        if (choice === userChoice) {
            msgArray.push('It\'s a tie! Please choose another.')
            return message.channel.send(msgArray.join('\n'))
        }

        switch (choice) {
        case 'rock':
            switch (userChoice) {
            case 'paper':
                msgArray.push('Paper wins!')
                break
            case 'scissors':
                msgArray.push('Rock wins!')
                break
            default:
                msgArray.push('Rock wins!')
            }
            break
        case 'paper':
            switch (userChoice) {
            case 'rock':
                msgArray.push('Paper wins!')
                break
            case 'scissors':
                msgArray.push('Scissors win!')
                break
            default:
                msgArray.push('Paper wins!')
            }
            break
        case 'scissors':
            switch (userChoice) {
            case 'rock':
                msgArray.push('Rock wins!')
                break
            case 'paper':
                msgArray.push('Scissors win!')
                break
            default:
                msgArray.push('Scissors win!')
            }
            break
        default:
            return Promise.resolve()
        }

        return message.channel.send(msgArray.join('\n'))
    }
}