/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')
const { stripIndents } = require('common-tags')
const { shuffle, verify } = require('../../utils/Helper')

module.exports = class RussianRouletteCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'russian-roulette',
            aliases: ['r-roulette', 'russia-gun', 'rr'],
            group: 'fun',
            memberName: 'russian-roulette',
            description: 'Who will pull the trigger and die first?',
            args: [
                {
                    key: 'opponent',
                    prompt: 'What user would you like to play against?',
                    type: 'user',
                    default: () => this.client.user,
                },
            ],
        })
    }

    async run(message, { opponent }) {
        if (opponent.id === message.author.id) return message.reply('Are you dumb? You cannot challenge yourself.')
        const current = this.client.games.get(message.channel.id)
        if (current) return message.reply(`Please wait until the current game of \`${current.name}\` is finished.`)
        this.client.games.set(message.channel.id, { name: this.name })
        try {
            if (!opponent.bot) {
                await message.say(`${opponent}, do you accept this challenge?`)
                const verification = await verify(message.channel, opponent)
                if (!verification) {
                    this.client.games.delete(message.channel.id)
                    return message.say('Looks like they declined...')
                }
            }
            let userTurn = true
            const gun = shuffle([true, false, false, false, false, false, false, false])
            let round = 0
            let winner = null
            let quit = false
            while (!winner) {
                const player = userTurn ? message.author : opponent
                const notPlayer = userTurn ? opponent : message.author
                if (gun[round]) {
                    await message.say(`**${player.tag}** pulls the trigger... **And dies!**`)
                    winner = notPlayer
                }
                else {
                    await message.say(stripIndents`
						**${player.tag}** pulls the trigger... **And lives...**
						${opponent.bot ? 'Continue?' : `Will you take the gun, ${notPlayer}?`} (${8 - round - 1} shots left)
					`)
                    const keepGoing = await verify(message.channel, opponent.bot ? message.author : notPlayer)
                    if (!keepGoing) {
                        if (opponent.bot) winner = opponent
                        else winner = player
                        quit = true
                    }
                    round++
                    userTurn = !userTurn
                }
            }
            this.client.games.delete(message.channel.id)
            if (quit) return message.say(`${winner} wins, because their opponent was a coward.`)
            return message.say(`The winner is ${winner}!`)
        }
        catch (err) {
            this.client.games.delete(message.channel.id)
            throw err
        }
    }
}