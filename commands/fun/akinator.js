/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')
const { MessageEmbed } = require('discord.js')
const { Aki, regions } = require('aki-api')
const { stripIndents } = require('common-tags')
const { list, verify } = require('../../utils/Helper')

module.exports = class AkinatorCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'akinator',
            aliases: ['aki'],
            group: 'fun',
            memberName: 'akinator',
            description: 'Think about a real or fictional character, I will try to guess who it is.',
            details: `**Regions:** ${regions.join(', ')}`,
            clientPermissions: ['EMBED_LINKS'],
            credit: [
                {
                    name: 'Akinator',
                    url: 'https://en.akinator.com/',
                    reason: 'Base API',
                },
            ],
            args: [
                {
                    key: 'region',
                    prompt: `What region do you want to use? Either ${list(regions, 'or')}.`,
                    type: 'string',
                    default: 'en',
                    oneOf: regions,
                    parse: region => region.toLowerCase(),
                },
            ],
        })
    }

    async run(message, { region }) {
        const current = this.client.games.get(message.channel.id)
        if (current) return message.reply(`Please wait until the current game of \`${current.name}\` is finished.`)
        try {
            const aki = new Aki(region, !message.channel.nsfw)
            let ans = null
            let win = false
            let timesGuessed = 0
            let guessResetNum = 0
            let wentBack = false
            let forceGuess = false
            const guessBlacklist = []
            this.client.games.set(message.channel.id, { name: this.name })
            while (timesGuessed < 3) {
                if (guessResetNum > 0) guessResetNum--
                if (ans === null) {
                    await aki.start()
                }
                else if (wentBack) {
                    wentBack = false
                }
                else {
                    try {
                        await aki.step(ans)
                    }
                    catch {
                        await aki.step(ans)
                    }
                }
                if (!aki.answers || aki.currentStep >= 79) forceGuess = true
                const answers = aki.answers.map(answer => answer.toLowerCase())
                answers.push('end')
                if (aki.currentStep > 0) answers.push('back')
                await message.say(stripIndents`
					**${aki.currentStep + 1}.** ${aki.question} (${Math.round(Number.parseInt(aki.progress, 10))}%)
					${aki.answers.join(' | ')}${aki.currentStep > 0 ? ' | Back' : ''} | End
				`)
                const filter = res => res.author.id === message.author.id && answers.includes(res.content.toLowerCase())
                const msgs = await message.channel.awaitMessages(filter, {
                    max: 1,
                    time: 30000,
                })
                if (!msgs.size) {
                    await message.say('Sorry, time is up!')
                    win = 'time'
                    break
                }
                const choice = msgs.first().content.toLowerCase()
                if (choice === 'end') {
                    forceGuess = true
                }
                else if (choice === 'back') {
                    if (guessResetNum > 0) guessResetNum++
                    wentBack = true
                    await aki.back()
                    continue
                }
                else {
                    ans = answers.indexOf(choice)
                }
                if ((aki.progress >= 90 && !guessResetNum) || forceGuess) {
                    timesGuessed++
                    guessResetNum += 10
                    await aki.win()
                    const guess = aki.answers.filter(g => !guessBlacklist.includes(g.id))[0]
                    if (!guess) {
                        await message.say('I can\'t think of anyone.')
                        win = true
                        break
                    }
                    guessBlacklist.push(guess.id)
                    const embed = new MessageEmbed()
                        .setColor(0xF78B26)
                        .setTitle(`I'm ${Math.round(guess.proba * 100)}% sure it's...`)
                        .setDescription(stripIndents`
							${guess.name}${guess.description ? `\n_${guess.description}_` : ''}
							_**Type [y]es or [n]o to continue.**_
						`)
                        .setThumbnail(guess.absolute_picture_path || null)
                        .setFooter(forceGuess ? 'Final Guess' : `Guess ${timesGuessed}`)
                    await message.embed(embed)
                    const verification = await verify(message.channel, message.author)
                    if (verification === 0) {
                        win = 'time'
                        break
                    }
                    else if (verification) {
                        win = false
                        break
                    }
                    else if (timesGuessed >= 3 || forceGuess) {
                        win = true
                        break
                    }
                    else {
                        await message.say('Hmm... Is that so? I can keep going!')
                    }
                }
            }
            this.client.games.delete(message.channel.id)
            if (win === 'time') return message.say('I guess your silence means I have won.')
            if (win) return message.say('Bravo, you have defeated me.')
            return message.say('Guessed right one more time! Loved playing Akinator with you!')
        }
        catch (err) {
            this.client.games.delete(message.channel.id)
            return message.reply(`Oh crap, an error occurred: \`${err.message}\`. Try again later!`)
        }
    }
}