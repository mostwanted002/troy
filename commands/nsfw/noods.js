/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')
const { MessageEmbed } = require('discord.js')
const { redditClientToken } = require('../../utils/redditClient.js')
const fetch = require('node-fetch')
const { noods } = require('../../data/noods.json')

// noinspection SpellCheckingInspection
module.exports = class NoodsCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'noods',
            description: 'Provides Dishwasher NSFW Content',
            group: 'nsfw',
            memberName: 'nudes',
            nsfw: true,
            examples: ['mute <user>'],
            aliases: ['nsfw', 'gulabi', 'nudes'],
            throttling: { usages: 1, duration: 5 },
            args: [
                {
                    key: 'search',
                    prompt: 'What do you want to see?',
                    type: 'string',
                },
            ],
            credit: [{
                name: 'Mayank Malik',
                url: 'https://gitlab.com/mostwanted002',
                reason: 'Codebase',
            }],
            clientPermissions: ['EMBED_LINKS'],
        })

    }

    async run(message, { search }) {
        const nsfwRole = message.guild.roles.cache.find(role => role.name === 'Ashleel')
        if (!message.member.roles.cache.has(nsfwRole.id)) return message.channel.send('Shushh! you normie underage aggiN. NOT ALLOWED by the orders of Pitamaha! 😤')

        const found = noods.find(function(element) {
            return element.name.includes(search.toLowerCase())
        })
        if (!found) return message.channel.send('Sorry, That category is not in database.')

        const token = await redditClientToken()
        let lucky = Math.floor(Math.random() * Math.floor(found.value.length))
        const nood = await fetch('https://oauth.reddit.com/' + found.value[lucky], {
            headers: {
                'Authorization': token,
                'Accept': 'application/json',
            },
        }).then(response => response.json())
        lucky = 0
        while (lucky === 0) {
            lucky = Math.floor(Math.random() * Math.floor(nood.data.children.length))
        }
        const noodEmbed = new MessageEmbed()
            .setColor('RANDOM')
            .setTitle('Hehehe here\'s some ' + found.fullName)
            .setDescription('Stay horny 🥵')
            .setImage(nood.data.children[lucky].data.url)
            .setFooter(`Powered by ${message.client.user.username}`, message.client.user.avatarURL())
            .setTimestamp()
        await message.react('😈').then(() => {
            message.channel.send(noodEmbed).catch(console.error)
        })

    }
}