/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')
const request = require('node-superfetch')
const semver = require('semver')
const { stripIndents } = require('common-tags')
const { dependencies, devDependencies } = require('../../package')

module.exports = class DependencyUpdateCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'dependency-update',
            aliases: ['dep-update', 'dependencies-update', 'npm-update'],
            group: 'util',
            memberName: 'dependency-update',
            description: 'Checks for dependency updates.',
            details: 'Only the bot owner(s) may use this command.',
            ownerOnly: true,
            guarded: true,
            credit: [
                {
                    name: 'npm',
                    url: 'https://www.npmjs.com/',
                    reason: 'API',
                },
            ],
        })
    }

    async run(msg) {
        const needUpdate = []
        for (const [dep, ver] of Object.entries(dependencies)) {
            const update = await this.parseUpdate(dep, ver)
            if (!update) continue
            needUpdate.push(update)
        }
        for (const [dep, ver] of Object.entries(devDependencies)) {
            const update = await this.parseUpdate(dep, ver)
            if (!update) continue
            needUpdate.push(update)
        }
        if (!needUpdate.length) return msg.say('All packages are up to date.')
        const updatesList = needUpdate.map(pkg => {
            const breaking = pkg.breaking ? ' ⚠️' : ''
            return `${pkg.name} (${pkg.oldVer} -> ${pkg.newVer})${breaking}`
        })
        return msg.say(stripIndents`
			__**Package Updates Available:**__
			${updatesList.join('\n')}
		`)
    }

    async fetchVersion(dependency) {
        const { body } = await request.get(`https://registry.npmjs.com/${dependency}`)
        if (body.time.unpublished) return null
        return body['dist-tags'].latest
    }

    async parseUpdate(dependency, version) {
        if (version.startsWith('github:')) return null
        const latest = await this.fetchVersion(dependency)
        const clean = version.replace(/^(\^|<=?|>=?|=|~)/, '')
        if (latest === clean) return null
        return {
            name: dependency,
            oldVer: clean,
            newVer: latest,
            breaking: !semver.satisfies(latest, version),
        }
    }
}
