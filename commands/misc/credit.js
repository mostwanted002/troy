/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')
const { Command: CommandoCommand } = require('discord.js-commando')
const { MessageEmbed } = require('discord.js')
const { trimArray, embedURL } = require('../../utils/Helper')

module.exports = class CreditCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'credit',
            group: 'misc',
            memberName: 'credit',
            aliases:['credits'],
            description: 'Responds with a command\'s credits list.',
            guarded: true,
            args: [
                {
                    key: 'command',
                    prompt: 'Which command would you like to view the credits list of?',
                    type: 'command|string',
                },
            ],
        })
    }

    run(message, { command }) {
        if (command instanceof CommandoCommand) {
            if (!command.credit) return message.say('This command is credited to no one. It just appeared.')
            const embed = new MessageEmbed()
                .setTitle(command.name)
                .setColor('GOLD')
                .setDescription(command.credit.map(credit => {
                    if (!credit.reasonURL) return `${embedURL(credit.name, credit.url)} (${credit.reason})`
                    return `${embedURL(credit.name, credit.url)} (${embedURL(credit.reason, credit.reasonURL)})`
                }).join('\n'))
            return message.embed(embed)
        }
        const cmd = command.toLowerCase()
        const commands = this.client.registry.commands
            .filter(c => c.credit && c.credit.length && c.credit.find(cred => cred.name.toLowerCase().includes(cmd)))
            .map(com => {
                const credit = com.credit.find(cred => cred.name.toLowerCase().includes(cmd))
                if (!credit.reasonURL) return `\`${com.name}\`: ${embedURL(credit.name, credit.url)} (${credit.reason})`
                return `\`${com.name}\`: ${embedURL(credit.name, credit.url)} (${embedURL(credit.reason, credit.reasonURL)})`
            })
        if (!commands.length) return message.say('Could not find any results.')
        const embed = new MessageEmbed()
            .setTitle(cmd)
            .setColor('GOLD')
            .setDescription(trimArray(commands, 15).join('\n'))
        return message.embed(embed)
    }
}