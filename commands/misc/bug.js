/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')
const { MessageEmbed } = require('discord.js')
const { developersRoleId } = require('../../config.json')

module.exports = class BugReportCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'bug',
            description: 'Sends bug report to developers.',
            group: 'misc',
            throttling: { usages: 1, duration: 3600 },
            memberName: 'bug-report',
            examples: ['bug <bug report>'],
            args: [{
                key: 'bugReport',
                prompt: 'Please define the bug with all possible details.',
                type: 'string',
            }],
            clientPermissions: ['EMBED_LINKS'],
        })
    }

    run(message, { bugReport }) {

        const bugChannel = message.guild.channels.cache.find(channel => channel.name === 'troy-bug')
        if (!bugChannel) return message.channel.send('Can not find the bug channel. Something is wrong.')

        const bugEmbed = new MessageEmbed()
            .setColor('RED')
            .setTitle('Bug Report')
            .setDescription(`Reported by ${message.author.username}(#${message.author.discriminator})`)
            .addField('Description', bugReport, false)
            .addField('Server Name', message.guild.name, true)
            .addField('Server ID', message.guild.id, true)
            .addField('🐞', `\n<@&${developersRoleId}> Please look into it.`, false)
            .setFooter(`Powered by ${message.client.user.username}`, message.client.user.avatarURL())
            .setTimestamp()

        return bugChannel.send(bugEmbed).then(() => {
            message.reply('Thanks for submitting a bug.').catch(console.error)
        })
    }

}