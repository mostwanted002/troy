/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')
const moment = require('moment')
const { shorten } = require('../../utils/Helper')

module.exports = class RemindCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'remind',
            aliases: ['timer', 'remind-me'],
            group: 'reminders',
            memberName: 'remind',
            description: 'Sets a reminder.',
            args: [
                {
                    key: 'time',
                    prompt: 'What do you want me to remind you about, and in how long?',
                    type: 'sherlock',
                },
            ],
        })
    }

    async run(message, { time }) {
        const exists = await this.client.timers.exists(message.channel.id, message.author.id)
        if (exists) return message.reply('🕰️ Only one reminder can be set per channel per user.')
        const timeMs = time.startDate.getTime() - Date.now()
        if (timeMs > 0x7FFFFFFF) return message.reply('🕰️ Reminders have a maximum length of ~24.84 days.')
        const display = moment().add(timeMs, 'ms').fromNow()
        const title = time.eventTitle ? shorten(time.eventTitle, 500) : 'something'
        await this.client.timers.setTimer(message.channel.id, timeMs, message.author.id, title)
        return message.say(`🕰️ Okay, I will remind you **"${title}"** ${display}.`)
    }
}